## Prima lezione

### Sintassi

#### consult

legge da file

```prolog
consult(nome_file).
```

oppure

```prolog
[nome file]
```

#### goal

Iniziano per la lettera maiuscola

```prolog
padre(luigi,X).
```

Va a prendere tutti quelli che hanno padre e luigi, al posto della x ci può essere qualciasi altra cosa

Quel comando dà solo una risposta, se ce ne sono di più, per vedere anche le altre basta mettere il ; invece di andare a capo dopo che è stata stampata la risposta.

I goal possono anche essere multipli.

```prolog
padre(luigi,X), madre(carla,X)
```

chiede se c'è un individuo X che è figlio di luigi e carla. X è sempre lo stesso individuo, non cambia tra la prima e la seconda parte della formula.

Si potrebbe scrivere dicendo che cerco X che abbia come padre Luigi e Y che abbia come madre Carla, e che X=Y. Tutto questo si abbrevia usando X in entrambe le parti.

#### regole

non c'è bisogno di rappresentare in modo esplicito l'OR, perchè basta creare più regole.

```prolog
genitore(X,Y):- padre(X,Y).
genitore(X,Y):- madre(X,Y).
```

le due parti destre in verità sono in OR tra di loro.

Nelle regole prolog il conseguente è a sinistra. In questo caso, se X è padre di Y, allora X è genitore di Y. (idem per la madre).

La variabile X **nella stessa regola** rappresenta lo stesso individuo. MA la variabile X **in regole diverse** può rappresentare due qualsiasi individui.

E' come se di fronte alla regola ci fosse un *per ogni X*.

Programmando in prolog, X e Y è come se fossero i parametri di una procedura. Lo scope infatti è la regola stessa.

#### ricorsione

Per definire la regola antenato, la regola base è che X sia padre di Y, ma per i gradi più alti bisogna introdurre la ricorsione.

questa è la definizione ricorsiva di antenato

```prolog
    antenato(X,Y):- genitore(X,Y).
    antenato(X,Y):- genitore(Z,Y), antenato(X,Z).
```

L'interprete del prolog legge le regole **nell'ordine in cui le sono state scritte**. In questo caso la regola di terminazione va prima così se riesce a fermarsi si ferma.

La ricerca quindi si sviluppa su un albero in profondità. Quando una regola fallisce, torna indietro e prova un'altra alternativa.

#### dif

questo predicato dice che le variabili passate come parametri devono essere individui diversi.

```prolog
sorella(X,Y):- femmina(X), genitore(Z,X), genitore(Z,Y), dif(X,Y).
```

In questo caso X e Y devono essere diversi perchè una persona non può essere sorella di sè stessa.

### Clausole di Horn

Le regole in prolog sono definite in modo logico così

```prolog
A <- B1 and B2 and B3 and ... and Bn
```

diventa

```prolog
A :- B1,B2,B3 ... Bn
```

dove A, B1,B2...Bn sono predicati.

Le regole in prolog sono clausole di Horn. Le clausole di horn sono clausole in cui c'è **al massimo un letterale positivo**.

Normalmente in logica una formula è una *clausola* se è fatta in questo modo:

```prolog
C1 or C2 or C3 or .... or Cn
```

Noi in prolog le chiamiamo cmq clausole di Horn perchè sono riconducibili ad esse.

Utilizzando la regola che 

```prolog
A <- B
```

può essere scritta come

```prolog
A or not(B)
```

allora la nostra regola iniziale (utilizzata in proolog)

```prolog
A <- B1 and B2 and B3 and ... and Bn
```

può essere trasformata in

```prolog
A or not(B1 and B2 and ... and Bn)
```

e poi

```prolog
A or not(B1) or not(B2) or not(B3) or ... or not(B4)
```

che è una clausola di Horn.

Per quanto riguarda il significato del goal, anch'esso è come il corpo di una clausola (la parte a destra di *A <-*)

#### Formule atomiche

```prolog
P(t1,...,tn)
```

è una formula atomica. (quindi i fatti in prolog sono formule atomiche). I *termini* del predicato t1,..,tn devono essere espressi in minuscolo.

I **termini** possono essere:

* delle *costanti*, anche detti *atomi*, che in prolog sono gli identificatori con la lettera minuscola, oppure dei numeri. I numeri in prolog non hanno praticamente nessun significato. 25 significa il carattere 2 seguito dal carattere 5.
* delle *variabili*, che in prolog sono gli identificatori con la lettera maiuscola
* dei *termini composti* Utilizzano un **simbolo di funzione** applicato ad un'altra lista di termini. Questi simboli di funzioni vengono spesso utilizzati in prolog per le strutture dati, come alberi o liste.

## Lezione persa

## 27 marzo

### Liste

Le **funzioni** in prolog si scrivono con il punto. Per esempio una lista:

```prolog
.(a,.(b,.(c,[])))
```

[] è un unico token, che indica la lista vuota.

Prolog poi traduce questa notazione in una notazione più semplice

```prolog
[a,b,c]
```

Se la funzione non è una lista

```prolog
.(a,b)
```

Prolog la traduce così

```prolog
[a|b]
```

Ci sono due tipi di liste: liste vuote [] e liste non vuote [H|T], [a,b,c].

La cosa importante da ricordare è che la lista non vuota ha sempre un primo elemento, la testa.

Esistono anche situazioni miste, per esempio [a,b|T], cioè una lista in cui a è la testa, b è il secondo elemento, e poi T è il resto della lista (la coda).

Vedere liste.md nella cartella 1-liste

Per sapere se un termine è una lista, dobbiamo chiederci se

1. è una lista vuota
2. è una lista non vuota [H|T], dove T è una lista

#### member

definiamo un predicato member, che è vero se il primo argomento è un elemento della lista (il secondo elemento deve essere una lista)

```prolog
member(T,[T|_]).
member(T,[_|L]) :- member(T,L).
```

La prima riga controlla se T è il primo elemento della lista
La seconda riga è la ricorsione: se T non è il primo elemento della lista, cerco se T è membro della coda.

**member** è in verità un predicato built-in del prolog

esempio di utilizzo di member

```prolog
7 ?- member(X,[a,b,c]).
X = a ;
X = b ;
X = c.

8 ?- member(X,[a,b,c]), member(X,[d,c]).
X = c.
```

il comando 7 restituisce tutti gli elementi che sono member di [a,b,c]

il comando 8 restituisce tutti gli elementi che sono sia member di [a,b,c] che di [d,c], cioè l'elemento c.

#### append

definiamo ora un predicato append(L1,L2,L)

    L1 è [a,b,c]
    L2 è [d,e]
    L diventa [a,b,c,d,e]

```prolog
append([], L2, L2).
append([H|T], L2, [H|T1]) :- append(T, L2, T1).
```

Il primo passo è semplice: se concateno la lista vuota a L2 ottengo L2

Il secondo passo è più complicato, **WAAAAT?** 

Esempi di append in prolog

```prolog
10 ?- append([1,2,3],[4,5],X).
X = [1, 2, 3, 4, 5].

11 ?- append(X,Y,[1,2,3,4,5]).
X = [],
Y = [1, 2, 3, 4, 5] ;
X = [1],
Y = [2, 3, 4, 5] ;
X = [1, 2],
Y = [3, 4, 5] ;
X = [1, 2, 3],
Y = [4, 5] ;
X = [1, 2, 3, 4],
Y = [5] ;
X = [1, 2, 3, 4, 5],
Y = [] ;
```

Attenzione: per usare append, tutti gli elementi devono essere **liste**!

Per esempio

```prolog
12 ?- append([1,2,3],4,X).
X = [1, 2, 3|4].
```

Succede perchè 4 non è una lista ma un numero. Bisognava usare [4].

#### I numeri

Usando i numeri si esce dalla programmazione logica. Ci sono in prolog, ma sono un'estensione non logica del linguaggio.

**Espressioni aritmetiche**

Se lego X a 2+3, il risultato non cambia molto:

```prolog
14 ?- X= +(2,3).
X = 2+3.
```

Questo perchè in questo caso i numeri non hanno alcun significato. Sono termini esattamente come tutti gli altri. Se chiedo a prolog se 2+3 è uguale a 5, mi dice di no.

C'è un modo per forzare prolog a valutare le espressioni aritmetiche, usando la parola chiave **is**. Però così facendo, si esce dal campo della logica.

```prolog
15 ?- X is 2+3.
X = 5.
```

Usando le espressioni aritmetiche è imporante l'ordine (dato che siamo fuori dalla logica) con cui vengono valutati i sottogoal:

```prolog
16 ?- Y=5, X is 2+Y.
Y = 5,
X = 7.

17 ?- X is 2+Y, Y=5.
ERROR: is/2: Arguments are not sufficiently instantiated
```

Esercizi proposti: guardare su moodle.

## 10 Aprile

Continuazione sulle **Liste**

#### reverse

```prolog
reversen([],[]).
reversen([H|T],LR) :- reversen(T,TR), append(TR,[H],LR)
```

Nella seconda riga, prendo il reverse di T (che è la lista iniziale tolta la testa) e ottengo TR, poi ci appendo la testa, e ottengo LR, che è la lista invertita.

Dentro reverse, c'è una chiamata ricorsiva di reversen, ma c'è anche una chiamata ad append, che a sua volta è ricorsiva. La append non tocca mai la seconda lista, ma è ricorsiva sulla prima lista. Scende ricorsivamente nella prima lista fino all'ultimo elemento, appende la seconda lista, e poi risale di nuovo la prima lista "ricostruendola".

C'è una soluzione più efficiente. Come lo faremmo in un linguaggio imperativo? Il modo + semplice per una lista `[a,b,c]` sarebbe:

1. `[a,b,c] []`
2. `[b,c] [a]`
3. `[c] [b,a]`
4. `[] [c,b,a]`

Se usassimo un linguaggio imperativo avremmo 2 variabili. In prolog non si può fare, dovremmo utilizzare un metodo ricorsivo. L'idea è quella di chiamare una chiamata ricorsiva che simula l'iterazione sopra utilizzando due parametri, più un terzo che è il risultato. Possiamo vederla così (anche se in prolog poi si usano le variabili, questa è solo una visualizzazione)

1. `rev_iter([a,b,c], [], R)`
2. `rev_iter([b,c], [a], R)`
3. `rev_iter([c], [b,a], R)`
4. `rev_iter([], [c,b,a], R)`

```prolog
rev_iter([],Res,Res).
rev_iter([H|T], ACC, Res) :- rev_iter(T,[H|ACC], Res).
reverse2(L,LR) :- rev_iter(L,[],LR)
```

Res è sempre lo stesso, perchè è il risultato, che dall'inizio alla fine è sempre lo stesso. ACC è un accumulatore. A differenza della reversen, la rev_iter chiama solo sè stessa, quindi la ricorsione che c'è è una ricorsione di coda, quindi è comunque più efficiente.

Questo è come funziona: (metto il nome della regola applicata a sinistra e a destra una visualizzazione di ciò che succede)

    reverse2([1,2,3,4],LR)
    rev_iter([1,2,3,4],[],LR)
    rev_iter([2,3,4],[1],LR)
    rev_iter([3,4],[2,1],LR)
    rev_iter([4],[3,2,1],LR)
    rev_iter([],[4,3,2,1],LR)
    rev_iter([],[4,3,2,1],[4,3,2,1])
    da questo punto, dato che ha trovato che LR = [4,3,2,1], risale le chiamate legando LR, fino a trovare
    reverse2([1,2,3,4],[4,3,2,1])

Suggerimento: implementare una funzione che determina se una lista è palindroma.

#### permut

```prolog
permut([],[]).
permut(List, [Element | Permutation]) :-
    select(Element, List, Rest),
    permut(Rest, Permutation)
```

select è fornito da prolog, e seleziona un elemento da una lista, mettendo il resto della lista nel terzo elemento. `select(X,[1,2,3,4],R)` non è deterministico, ma prolog lo trasforma in deterministico facendo backtracking e ritornando in ordine le diverse possibilità.

```prolog
?- select(X,[a,b,c],L).
X = a,
L = [b, c] ;
X = b,
L = [a, c] ;
X = c,
L = [a, b] ;
```

#### int (intersezione tra insiemi)

```prolog
int([],S2,[]).
int([X|REST],S2,[X|REST1]) :- member(X,S2), int(REST,S2,REST2).
int([_|REST],S2,REST1) :- int(REST,S2,REST1).
```

Spiegazione delle 3 regole:

1. Intersezione tra insieme vuoto e insieme pieno -> insieme vuoto
2. Se X appartiene al seconda insieme, allora fa parte del risultato e lo aggiungo in testa (dato che apparteneva alla prima insieme).
3. Altrimenti, X (in questo caso _) non fa parte del risultato quindi non lo inserisco ma lo tolgo dalla prima lista per andare avanti di un passo ricorsivo. In questo caso sfruttiamo l'ordine delle regole: se member fallisce, allora fallisce anche la seconda regola; prolog fa backtracking e va a prendere la terza regola, che è sempre applicabile.

Dal punto di vista formale questo programma non è tanto corretto, perchè nell'ultima regola dovrebbe esserci scritto "not member". Invece è stato omesso perchè ci affidiamo all'ordine con cui prolog sceglie le regole e fa backtracking.

Infatti non funziona. O meglio, funziona per il primo risultato, ma se lo forziamo a fare backtracking con il ; va a valutare l'ultima regola e troveremo diversi risultati in cui man mano ci sono sempre meno elementi

##### cut (!)

Prolog mette a disposizione **cut**, che è un meccanismo computazionale (non c'entra nulla con la logica) built-in che si chiama con il punto esclamativo (!). Il **cut** praticamente taglia via i percorsi di backtracking.

Quindi una soluzione migliore usando ! può essere

```prolog
int([],S2,[]).
int([X|REST],S2,[X|REST1]) :- member(X,S2), !, int(REST,S2,REST2).
int([_|REST],S2,REST1) :- int(REST,S2,REST1).
```

Questo meccanismo funziona così:

    p :- q1,...qi,!,qi+1,...,qn.

Prolog è andato avanti fino a qi+qualcosa. Quando torna indietro, il cut taglia tutti i punti di scelta da q1 a qi, e anche di p.

quindi il cut dopo il member vuol dire che se member ha successo, si va avanti con int. Però appena il cut viene passato "da sinistra a destra", tutti i punti di scelta che c'erano vengono cancellati. In quel momento il punto di scelta aperto era la possibilità di chiamare la terza regola. In poche parole dal cut in poi la scelta che ho fatto non la posso più cambiare, tutto ciò che c'era prima non esiste più. 

#### Negazione

Se ho un goal G, posso anche avere un goal not G.

Il not ha una definizione computazionale operazionale. Se non derivo G, allora assumo che not G sia vero. Non è il not della logica classica.

#### Call

chiama una funzione. E' un modo per avere un modo per trattare i dati come fossero programmi.

## 16 aprile

### Strategia di ricerca

In prolog i predicati sono overloaded, cioe' ogni predicato e' identificato non solo dal nome ma anche dal numero di argomenti. Spesso infatti si hanno messaggi di errore del tipo `member/2`.

#### Ricerca in profondità

```prolog
% Ricerca in profondità

ric_prof(S,[]) :- finale(S), !.

ric_prof(S,[Az|Resto]) :-
  applicabile(Az,S),
  trasforma(Az,S,Nuovo_S),
  ric_prof(Nuovo_S,Resto).

prof :- iniziale(I), ric_prof(I,Ris), write(Ris).
```

Prima riga: se S è uno stato finale, allora ci fermiamo.

Dalla seconda riga: Se S non è uno stato finale, allora **non deterministicamente** vado a prendere un'azione applicabile, la applichiamo, passiamo nello stato Nuovo_S e andiamo avanti ricorsivamente

    S --- Az ---> Nuovo_S

Dal punto di vista logico non è deterministico, ma il Prolog lo rende deterministico perchè decide quale azione applicabile utilizzare in base all'ordine con cui sono state inserite.

La **ricerca in profondità** quasi sicuramente **non trova** la soluzione **ottima**

Il **cut** `!` serve nel caso in cui volessimo trovare altre soluzioni. Se non ci fosse, quando usiamo ; per forzare il fallimento e fare backtracking, il programma si trovava già nello stato finale e aveva due possibilità.

1. il primo ric_prof, che è stato appena eseguito per arrivare allo stato finale
2. il secondo ric_prof, che fa fare un altro passo

Senza il cut, usando ; si forzerebbe il backtracking ad applicare la seconda alternativa partendo però dallo stato finale. Il risultato sarebbe che il programma va a trovare un'altra soluzione che però passa due volte dallo stato finale.

Il cut permette di eliminare la seconda scelta quando si fa backtracking. In poche parole fa in modo di tornare indietro allo stato precedente del goal, perchè in una situazione di questo tipo

    p :- a, !, b.
    p :- c

fa fallire tutto ciò che c'è prima, cioè a, ma anche p. quindi p fallendo va a richiamare chi l'aveva invocato prima, che è la situazione dello stato precedente a quello finale.


