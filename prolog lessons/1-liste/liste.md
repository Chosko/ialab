liste.pl contiene una lista.

    p([1,2,3,4,5])

Alcuni esempi di comandi prolog dopo aver caricato la lista

    1 ?- p(X).
    X = [1, 2, 3, 4, 5].

    2 ?- p([X|Y]).
    X = 1,
    Y = [2, 3, 4, 5].

    3 ?- p([X,Y|Z]).
    X = 1,
    Y = 2,
    Z = [3, 4, 5].

Se mi interessa solo il primo elemento della lista, mi basta usare il carattere underscore per la variabile che non mi interessa

    5 ?- p([X|_]).
    X = 1.