int([],Y,[]).
int([X|REST],Y,[X|REST1]) :- member(X,Y), int(REST,Y,REST2).
int([_|REST],Y,REST1) :- int(REST,Y,REST1).

intc([],Y,[]).
intc([X|REST],Y,[X|REST1]) :- member(X,Y), !, intc(REST,Y,REST2).
intc([_|REST],Y,REST1) :- intc(REST,Y,REST1).
