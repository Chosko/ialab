% comando
prof_cc_lim(2).

% passo 0
prof_cc_lim(MaxProf) :- 
     iniziale(I),
     ric_prof_cc_lim(I,[],MaxProf,0,Ris),
     write(Ris).

prof_cc_lim(2) :- 
     iniziale(pos(4,2)),
     ric_prof_cc_lim(pos(4,2),[],2,0,Ris),
     write(Ris).

% passo 1
ric_prof_cc_lim(S,Visitati,MaxProf,Prof,[Az|Resto]) :-
     applicabile(Az,S),
     trasforma(Az,S,Nuovo_S),
     \+ member(Nuovo_S,Visitati),
     MaxProf > Prof,
     ric_prof_cc_lim(Nuovo_S,[S|Visitati],MaxProf,Prof1,Resto),
     Prof1 is Prof+1.

ric_prof_cc_lim(pos(4,2),[],2,0,[est|Resto]) :-
     applicabile(est,pos(4,2)),
     trasforma(est,pos(4,2),pos(4,3)),
     \+ member(pos(4,3),[]),
     2 > 0,
     ric_prof_cc_lim(pos(4,3),[pos(4,2)|[]],2,1,Resto),
     1 is 0+1.

% passo 2
ric_prof_cc_lim(S,Visitati,MaxProf,Prof,[Az|Resto]) :-
     applicabile(Az,S),
     trasforma(Az,S,Nuovo_S),
     \+ member(Nuovo_S,Visitati),
     MaxProf > Prof,
     ric_prof_cc_lim(Nuovo_S,[S|Visitati],MaxProf,Prof1,Resto),
     Prof1 is Prof+1.

ric_prof_cc_lim(pos(4,3),[pos(4,2)],2,1,[sud|Resto]) :-
     applicabile(sud,pos(4,3)),
     trasforma(sud,pos(4,3),pos(5,3)),
     \+ member(pos(5,3),[pos(4,2)]),
     2 > 1,
     ric_prof_cc_lim(pos(5,3),[pos(4,3)|[pos(4,2)]],2,2,Resto),
     2 is 1+1.

% passo 3 (goal)

ric_prof_cc_lim(S,_,_,_,[]) :- finale(S),!.

ric_prof_cc_lim(pos(5,3),[pos(4,3),pos(4,2)],2,3,[]) :- finale(pos(5,3)),!.