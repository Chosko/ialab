% comando
prof_cc.

% passo 0
prof_cc :- 
     iniziale(pos(4,2)),
     ric_prof_cc(pos(4,2),[],Ris),
     write(Ris).

% passo 1

ric_prof_cc(S,Visitati,[Az|Resto]) :-
     applicabile(Az,S),
     trasforma(Az,S,Nuovo_S),
     \+ member(Nuovo_S,Visitati),
     ric_prof_cc(Nuovo_S,[S|Visitati],Resto).

ric_prof_cc(pos(4,2),[]],[est|Resto]) :-
     applicabile(est,pos(4,2)),
     trasforma(est,pos(4,2),pos(4,3)),
     \+ member(pos(4,3),[]]),
     ric_prof_cc(pos(4,3),[pos(4,2)|[]]],Resto).

% passo 2

ric_prof_cc(S,Visitati,[Az|Resto]) :-
     applicabile(Az,S),
     trasforma(Az,S,Nuovo_S),
     \+ member(Nuovo_S,Visitati),
     ric_prof_cc(Nuovo_S,[S|Visitati],Resto).

ric_prof_cc(pos(4,3),[pos(4,2)],[sud|Resto]) :-
     applicabile(sud,pos(4,3)),
     trasforma(sud,pos(4,3),pos(5,3)),
     \+ member(pos(5,3),[pos(4,2)]),
     ric_prof_cc(pos(5,3),[pos(4,3)|[pos(4,2)]],Resto).

% passo 3 (goal)

ric_prof_cc(pos(5,3),_,[]) :- finale(pos(5,3)),!.

% Risalgo al passo 2

ric_prof_cc(pos(4,3),[pos(4,2)],[sud|Resto]) :-
     applicabile(sud,pos(4,3)),
     trasforma(sud,pos(4,3),pos(5,3)),
     \+ member(pos(5,3),[pos(4,2)]),
     ric_prof_cc(pos(5,3),[pos(4,3)|[pos(4,2)]],Resto).

ric_prof_cc(pos(4,3),[pos(4,2)],[sud|[]]) :-
     applicabile(sud,pos(4,3)),
     trasforma(sud,pos(4,3),pos(5,3)),
     \+ member(pos(5,3),[pos(4,2)]),
     ric_prof_cc(pos(5,3),[pos(4,3)|[pos(4,2)]],[]).

% Risalgo al passo 1

ric_prof_cc(pos(4,2),[]],[est|Resto]) :-
     applicabile(est,pos(4,2)),
     trasforma(est,pos(4,2),pos(4,3)),
     \+ member(pos(4,3),[]]),
     ric_prof_cc(pos(4,3),[pos(4,2)|[]]],Resto).

ric_prof_cc(pos(4,2),[]],[est|sud]) :-
     applicabile(est,pos(4,2)),
     trasforma(est,pos(4,2),pos(4,3)),
     \+ member(pos(4,3),[]]),
     ric_prof_cc(pos(4,3),[pos(4,2)|[]]],[sud]).

% Risalgo al passo 0

prof_cc :- 
     iniziale(pos(4,2)),
     ric_prof_cc(pos(4,2),[],Ris),
     write(Ris).

prof_cc :- 
     iniziale(pos(4,2)),
     ric_prof_cc(pos(4,2),[],[est|sud]),
     write([est|sud]).

% Risalgo al comando

prof_cc.

[est,sud]