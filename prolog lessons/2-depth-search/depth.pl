% ricerca in profonditˆ

ric_prof(S,[]) :- finale(S), !.
ric_prof(S,[Az|Resto]) :-
     applicabile(Az,S),
     trasforma(Az,S,Nuovo_S),
     ric_prof(Nuovo_S,Resto).

prof :- iniziale(I), ric_prof(I,Ris), write(Ris).



% ricerca in profonditˆ con controllo cicli
% il secondo parametro contiene la lista degi stati
% visitati dallo stato iniziale fino ad S
  
ric_prof_cc(S,_,[]) :- finale(S),!.
ric_prof_cc(S,Visitati,[Az|Resto]) :-
     applicabile(Az,S),
     trasforma(Az,S,Nuovo_S),
     \+ member(Nuovo_S,Visitati),
     ric_prof_cc(Nuovo_S,[S|Visitati],Resto).

prof_cc :- iniziale(I), ric_prof_cc(I,[],Ris), write(Ris).


% ricerca in profondità con controllo cicli e profondità
% il secondo parametro contiene la lista degi stati
% visitati dallo stato iniziale fino ad S
  
ric_prof_cc_lim(S,_,_,_,[]) :- finale(S),!.
ric_prof_cc_lim(S,Visitati,MaxProf,Prof,[Az|Resto]) :-
     applicabile(Az,S),
     trasforma(Az,S,Nuovo_S),
     \+ member(Nuovo_S,Visitati),
     MaxProf > Prof,
     ric_prof_cc_lim(Nuovo_S,[S|Visitati],MaxProf,Prof1,Resto),
     Prof1 is Prof+1.

prof_cc_lim(MaxProf) :- iniziale(I), ric_prof_cc_lim(I,[],MaxProf,0,Ris), write(Ris).

% iterative_deepening
  
it_deep(S,_,[]) :- finale(S),!.
it_deep(S,Visitati,MaxProf,Prof1,[Az|Resto]) :-
     applicabile(Az,S),
     trasforma(Az,S,Nuovo_S),
     \+ member(Nuovo_S,Visitati),
     MaxProf > Prof,
     it_deep(Nuovo_S,[S|Visitati],MaxProf,Prof,Resto),
     Prof1 is Prof+1.

iterative_deepening :- iniziale(I), it_deep(I,[],Ris), write(Ris).

