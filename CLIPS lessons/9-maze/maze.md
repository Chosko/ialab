# Labirinto

Vediamo un metodo di ricerca su grafo per risolvere il problema del labirinto

Questo metodo è indipendente dal problema ma dipendente dal dominio: non dipende dal tipo di labirinto ma dipende dal fatto che serve per risolvere problemi di quel tipo.

Il sistema ha delle regole (azioni). 

### up-apply

```prolog
(defrule up-apply
        (declare (salience 50))
        (current ?curr)
        (node (ident ?curr) (pos-r ?r) (pos-c ?c) (open yes))
        (cell (pos-r =(+ ?r 1)) (pos-c ?c) (contains empty|gate))
   => (assert (apply ?curr up ?r ?c))
              )
```

Se siamo nel nodo corrente e questo nodo descrive lo stato in cui l'agente si trova in riga R e colonna C, questo stato è in OPEN, e la cella sopra è vuota, allora asserisco che è applicabile allo stato corrente un'azione up dalla cella R,C

### up-exec

```prolog
(defrule up-exec
        (declare (salience 50))
        (current ?curr)
        (lastnode ?n)
 ?f1<-  (apply ?curr up ?r ?c)
        (node (ident ?curr) (gcost ?g))
   => (assert (exec ?curr (+ ?n 1) up ?r ?c)
              (newnode (ident (+ ?n 1)) (pos-r (+ ?r 1)) (pos-c ?c) 
                       (gcost (+ ?g 1)) (father ?curr)))
      (retract ?f1)
      (focus NEW))

```


Esegue (nel mondo simbolico, non nel mondo fisico) l'azione up. Asserisce il fatto che l'azione è stata eseguita. Inoltre crea il nuovo nodo. Inventa un nuovo identificatore. 

### down-exec

```prolog
(defrule down-exec
        (declare (salience 50))
        (current ?curr)
        (lastnode ?n)
 ?f1<-  (apply ?curr down ?r ?c)
        (node (ident ?curr) (gcost ?g))
   => (assert (exec ?curr (+ ?n 2) down ?r ?c)
              (newnode (ident (+ ?n 2)) (pos-r (- ?r 1)) (pos-c ?c) 
                       (gcost (+ ?g 1)) (father ?curr)))
      (retract ?f1)
      (focus NEW))
```

Il nodo creato ha l'identificatore aumentato di 2. Lastnode è l'identificatore dell'ultimo nodo che stiamo espandendo. Il nuovo nodo in down è incrementato di 2 perchè altrimenti avrebbe lo stesso identificatore di up che è aumentato solo di 1.

Questo capita perchè noi non andiamo a modificare lastnode finchè non abbiamo finito di espandere lo stato corrente.

### Generazione dei nodi

Una cosa che deve fare il programma è controllare se un nodo è già stato generato. Se è già stato generato bisogna tenere quello vecchio, perchè stiamo facendo una ricerca in ampiezza, quindi se il nodo era stato creato prima vuol dire che era comunque meno profondo di quello corrente.