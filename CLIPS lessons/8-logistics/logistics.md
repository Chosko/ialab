Facendo partire il programma originale, CLIPS va in loop.
Si ottiene il plane p2 che si sposta ripetutamente da Roma a Parigi e viceversa.

Il problema è che nell'agenda la prima azione che si trova è sempre un'azione di move, quindi continua a spostare gli aerei, senza nè caricare nè scaricare

### LEZIONE PERSA


**apply-move1**: 
Serve per eliminare gli stati che erano stati visitati ma non portavano alla soluzione.
