# CLIPS

## Syntax

### assert

Asserisce nuovi fatti

    (assert (nonno luigi anna))

Il fatto viene creato solo se non esiste già

### retract

Elimina il fatto se esiste

    (retract (nonno luigi anna))

### deftemplate

Definisce fatti strutturati (non ordinati)

    (deftemplate studente
        (slot matricola)
        (slot nome)
        (slot eta (type integer)(range 0 100))
        (slot iscrittoa (allowed-values MAT INF CHIMICA)(default INF)))

I fatti non ordinati si istanziano con assert

    (assert (studente 
        (nome PIPPO)
        (eta 24)
        (iscrittoa MAT)))

Gli slot non definiti assumono valore nil oppure il default se è stato definito

Limitazione: non si possono definire slot che contengono a loro volta fatti non ordinati

### modify

Modifica un fatto, dato il suo id

    (modify 2 (matricola 2))

Questo modifica il fatto 2 mettendogli matricola 2.

> WARNING: sta roba modifica l'ID del fatto!!!!! 

### ?

Quando l'interprete trova un ?, vuol dire che qualunque elemento va bene per il matching.

    (data ?)

matcha tutti i fatti che iniziano con data e hanno ancora un elemento

> ? non matcha la **sequenza vuota**

### ?x

    (data ?x)

indica che x è una variabile che viene matchata dal secondo elemento del fatto data.

In quel momento se la variabile x ha matchato 'blue', diventa legata (bundled).
(Rileggersi l'algoritmo di unificazione dal libro di Russel e Norvig)

Dopo il matching, la variabile x non è più una variabile libera.

### ?$

Indica una qualunque sequenza.

    (data ? blue red $?)

* Il primo elemento deve essere data.
* Il secondo elemento può essere qualunque
* Il terzo deve essere 'blue'
* Il quarto deve essere 'red'
* L'ultimo può essere: una qualunque sequenza di valori, **anche la sequenza vuota**

### ?x&

    (person (age ?x&20|21|22))

matcha una persona con età 20, 21 o 22. L'età viene legata alla variabile x

### printout

stampa  un messaggio

    (printout t "?x = " ?x crlf
              "?y = " ?y crlf
              "?z = " ?z crlf
              "------" crlf))

### declare

    (defrule r1
        (declare (salience -1))
        (data 1))
        =>  (assert (data 2)))

la salience è la priorità della regola.
Questa regola viene utilizzata per ultima

## Clausole di Horn definite

E' una clausola in cui c'è esattamente un letterale positivo.

Vuol dire che le regole clausole definite possono essere:

    A // Singoletto
    not B or not C or not D or A // Solo un letterale positivo
    B and C and D -> A // 

???

    (defrule noderivation
        (declare (salience -1))
        (goal criminal ?x)
        (not (criminal ?x))
        =>  (printout t "criminal " ?x " non conseguenza logica KB"  crlf)
            (halt))

Questo not è un *negation by failure*, non è il not della logica classica

Significa: quella condizione è soddisfatta se NON è presente nell'insieme dei fatti un fatto che soddisfa quella condizione

    not A

è soddisfatto se non trovo A nei fatti (non vuol dire che è soddisfatto se trovo "not A")

## Criterio di inferenza

Di norma, in cima all'agenda viene messa la regola soddisfatta dai fatti più recenti.

Per modificare il criterio di inferenza, dal menù di clips: Execution > Options > Strategy.

* Depth (profondità): le regole soddisfatte dai fatti più recenti vanno in cima.
* Breathd (ampiezza): le regole soddisfatte dai fatti più recenti vanno in fondo.
* Complexity: privilegia le regole con più condizioni
* Simplicity: privilegia le regole con meno condizioni
* LEX e MEA: mantenute per retrocompatibilità

Domanda: se abbiamo gli stessi fatti, ma ordinati in modo diverso, l'insieme delle regole che CLIPS ritorna come attivabili, cambia? NO

Se noi assumiamo che le nostre regole fanno solo delle assert, l'insieme dei fatti crescerà in modo monotono.

## Planning

Se vogliamo risolvere dei problemi, in alcuni casi ci basta un'architettura di un agente semplice, basato sui riflessi. Ogni tanto però serve un agente che ragioni sul mondo per capire come arrivare al goal. E' possibile utilizzare un linguaggio come CLIPS per fare ciò?

Un agente si può descrivere con 3 elementi: **(S_0, G, A)**

* **S_0**: Uno stato iniziale. L'agente sa com'è fatta almeno una parte del mondo
* **G**: il goal. E' una configurazione che so di voler raggiungere
* **A**: una serie di azioni che l'agente può compiere
Il risultato è un **piano**, cioè (nella versione più semplice) una **sequenza di azioni** a_1...a_n

S_0 --a_1--> S_1 ---a_2---> S_2 -----> .... -----a_n---> S_n    dove S_n soddisfa il goal G

Per generare il piano, si possono aggiungere delle restrizioni, per esempio trovare il piano che utilizzi il minor numero di azioni possibile. In alcuni casi si aggiunge un costo alle azioni, per poter trovare il cammino di costo minimo.

Quello che dobbiamo chiederci per capire se questo modello si può sviluppare su CLIPS è:

* come modello S_0,G,A ?
* abbiamo un meccanismo inferenziale che possa risolvere questo tipo di problema?


### [STRIPS](http://it.wikipedia.org/wiki/STRIPS) è un sistema utilizzato per risolvere problemi nello spazio
Si usano predicati positivi.

**S_0**: lo stato iniziale è definito da tanti fatti, per esempio per il mondo dei blocchi

    on(A,B) on(C,D) ontable(D)

**Goal**: congiunzione di predicati. Per esempio nel mondo dei blocchi alla fine voglio ottenere

    on(A,C) on(C,B) on(B,D) ontable(D)

**Azioni istanziate (Ground)**

**Azioni con variabili/parametri**: hanno degli argomenti. Ogni azione è descritta da due parti: *precondizioni* ed *effetti*

    pickup(A,B)

* *precondizioni*: Specificano sotto quali condizioni l'azione è applicabile
* *effetti*: indicano quale sarà lo stato dopo che l'azione viene eseguita
    - ADD: fatti veri dopo l'esecuzione di A
    - DELETE: fatti non più veri dopo l'esecuzione di A

Nell'esempio più semplice ho uno stato S_i e un'azione A che mi porta allo stato S_i+1. L'azione è applicabile se le sue precondizioni sono verificate nello stato S_i. Gli effetti saranno verificati nello stato S_i+1

Supponiamo che nel mondo dei blocchi c'è il telecomando appoggiato sul tavolo. Eseguo l'azione picktable sul telecomando. Il nuovo stato è che il telecomando è ora sul braccio del robot. Inoltre il telecomando non è più appoggiato sul tavolo. C'è anche una nozione di *persistenza* perchè tutte le altre condizioni (per esempio, il computer è sul tavolo) rimangono invariate.

E' quindi compito di chi scrive l'azione capire cosa cambia e cosa non cambia.

### CLIPS

In clips i tre elementi (S_0, G, A) si possono definire con:

* **S_0**: lo stato iniziale è una (o più) deffacts
* **G**: il goal (o i goal) è una (o più) deffacts
* **A**: le azioni si definiscono come regole
    - Le regole vengono utilizzate con assert e retract. Problema: le retract hanno bisogno dell'indice del fatto da cancellare.

In CLIPS, si può salvare l'indice di un fatto in una variabile. Questo è utile per fare retract

    (defrule puttable
      ?t <- (holding ?x)
      => 
      (assert 
        (ontable ?x)
        (clear ?x)
        (handempty)))
      (retract ?t)

## Espansione

Quello che si chiama espansione (cap. 3 di Russel Norvig) viene fatto gratuitamente in CLIPS

## Relazione tra strategie di clips e strategie di ricerca (Russel e Norvig)

L'unico ruolo della strategia CLIPS è quella di fornire all'interprete CLIPS un criterio con cui ordinare le regole. Nel caso Depth i fatti più recenti vanno in cima. Il matching tra recentezza delle regole e strategia in profondità esiste solo se si utilizzano clausole di Horn definite, e si usa un ragionamento monotono, quindi i fatti vengono solo aggiunti.

Nel caso di Logistics.CLP le strategie di CLIPS non funzionano perchè non siamo nel caso del ragionamento non monotono.

### Logistics.CLP

Il mondo modellato è reversibile, vuol dire che ogni azione che abbiamo modellato ha un'azione inversa (per esempio load e unload). Lo stesso vale per il mondo dei blocchi.

L'idea è di fare in modo che se il percorso che sto seguendo non funziona, posso esplorare le varie possibilità (in ampiezza o profondità) eseguendo delle operazioni all'inverso. In questo modo potrei effettivamente fare una ricerca su un albero. Prima invece continuavo a creare nuovi stati.

Quando facciamo una strategia di ricerca o un planning, noi non stiamo ancora operando nel mondo, ma stiamo facendo finta (in un mondo completamente simulato) di svolgere le azioni per capire qual'è la strada migliore.

Questa proprietà ci permette di esplorare delle strade (anche con un grosso costo computazionale) senza eseguire azioni nel mondo reale. Quando pianifico un viaggio non compro un biglietto ogni volta che "simulo" il viaggio. Faccio tutte le prove e poi quando ho il piano compro i biglietti che mi servono.

In CLIPS per fare questo dovremmo mantenere traccia degli stati che sono già stati visitati. Di norma abbiamo solo traccia dello stato corrente tra i nostri fatti.

Russel Norvig dicono:

    RISULTATO(a,s) = 
        (s - DEL(a)) U ADD(a)

In pratica, il nuovo stato è il vecchio stato s meno le cancellazioni date dall'azione a più le aggiunte dell'azione a.

Si può fare in tanti modi diversi. Una delle soluzioni è avere dei fatti ordinati:

    (status index empty p1 NA)
    (status index is_in c1 Vienna)

## 26 marzo - Planning (metodologia)

Vedere cap 3.10 di Russell Norvig

Per capire il planning bisogna definire il **planning problem**: (S_0, G, A)

come visto prima, il piano è una sequenza ordinata di operatori che mandano dallo stato iniziale allo stato finale, che soddisfa il goal

    S_0 --a_1--> S_1 ---a_2---> S_2 -----> .... -----a_n---> S_n    dove S_n soddisfa il goal G

Inizialmente si è partiti da STRIPS, linguaggio che prevede delle **azioni**, determinate da **precondizioni** e azioni di **add/delete**

Da STRIPS, dopo una lunga storia, si è arrivati oggi ad usare PDDL, che nella sua versione base è molto simile a STRIPS

Fino a questo punto del corso abbiamo usato come metodo di **pianificazione** la **ricerca nello spazio degli stati**

Ci sono due modi di ricercare nello spazio degli stati:

1. **Forward (progression) state-space search** Parto dallo stato iniziale, faccio una ricerca in avanti, e vedo se riesco ad arrivare allo stato finale. Il problema di questa strategia è che se non abbiamo un'**euristica** che ci guida per arrivare al goal, diventa molto complicato. E' stato molto sottovalutato per molti anni
2. **Backward (regression) state-space search** Parto dal goal usando le regole al contrario. Vedo se c'è un'azione che ha come effetto il portrami allo stato finale. Vado indietro finchè non trovo lo stato iniziale

Esempio: dominio del vestirsi. Da una situazione in cui non ho nè calze nè scarpe voglio arrivare uno stato finale in cui indosso sia calze che scarpe. Per un pianificatore come quello che abbiamo fatto per i blocchi si tratta di una ricerca di profondità 6, con tutte le soluzioni lunghe uguali.

    Start -> Right sock -> Left sock -> Right shoe -> Left shoe -> Finish
    Start -> Left sock -> Right sock -> Right shoe -> Left shoe -> Finish
    Start -> Right sock -> Left sock -> Left shoe -> Right shoe -> Finish
    Start -> Left sock -> Right sock -> Left shoe -> Right shoe -> Finish
    Start -> Left sock -> Left shoe -> Right sock -> Right shoe -> Finish
    Start -> Right sock -> Right shoe -> Left sock -> Left shoe -> Finish

Noi abbiamo fatto dei **total order plan**, cioè completi dall'inizio alla fine. Invece si possono fare dei **partial order plan** (**POP**), cioè piccoli piani parziali ottenuti tramite dei vincoli (per esempio non posso mettermi la scarpa destra prima della calza destra). Il vantaggio è che tutto ciò che non è specificato non va a impattare con il piano (per esempio posso mettere prima la calza destra e poi la sinistra o viceversa, senza alterare il risultato).

            -> Left Sock -> Left Shoe   ->
    Start                                   Finish
            -> Right Sock -> Right Shoe ->

Quindi l'idea di un piano come sequenza potrebbe non essere la cosa più furba da fare perchè possiamo trovare tante soluzioni diverse da loro, della stessa lunghezza, ma equivalenti. (scarpe e calze)

I **piani parzialmente ordinati** sono dei **DAG**, non più delle sequenze. In questo modo l'ordine con cui si segue il piano non è specificato in alcune sue parti. Si può poi passare al **piano totalmente ordinando** eseguendo un'operazione di **linearizzazione** del piano. In questo modo trasformo il **DAG** nelle sequenze che avrei ottenuto facendo la ricerca per il piano totalmente ordinato.

Un altro vantaggio dei **piani parzialmente ordinati** è che alcune delle azioni possono essere eseguite in concorrenza. Se avessi due esecutori, potrei fare che mentre infilo la calza destra, posso infilare anche la calza destra.

Altro dominio esempio: Come faccio a cambiare la ruota bucata? (figura 11.3 Russel Norvig 2 ed.)

Nello stato iniziale la ruota bucata (flat) è montata sull'asse, e c'è una ruota di scorta nel bagagliaio. Il goal è di avere la ruota di scorta montata sull'asse. Per risolvere questo problema ci sono delle azioni

Esercizio proposto: fare questa roba in CLIPS

**vincoli causali**: ci sono delle azioni che hanno come effetto l'asserzione di un fatto che serve immediatamente come precondizione di un'altra azione. Sono quelli per cui una certa azione mi abilita per poter attivare altre azioni.

### Proviamo a fare un piano.

Inizialmente ci sono le azioni Start e Finish. Queste sono delle **pseudo-azioni** perchè hanno delle particolarità: Non ci sono azioni che possono essere eseguite prima di Start, e non ci sono azioni che possono essere eseguite dopo Finish. Finish inoltre non ha nessun effetto, ma ha delle precondizioni. 
Le precondizioni di Finish sono i goal. Mentre gli effetti dello Start sono i fatti iniziali. Quindi questa pseudoazione fornisce tutti i fatti iniziali.

```                                                   
             S_0^1                       G_1            
            +---->                     +--------+       
            |                                   |       
    +-------+ S_0^2                      G_2   +v------+
    | Start +----->                    +------->Finish |
    +-------+                                  +^------+
            | S_0^3                      G_3    |       
            +----->                    +--------+       
```

#### Problemi di interferenza tra azioni, con una strategia backward

Utiliziamo una strategia backward, per risolvere il problema della ruota.

Partendo da finish, l'unica cosa che mi porta al goal è PutOn(Spare,Axle). Poi mi serve: At(Spare, Ground) e not At(Flat,Axle)

Cercando le azioni che mi fanno ottenere queste regole ci sono (tra le altre):

* Remove(Spare,Trunk), che ha come precondizione At(Spare,Trunk) e come effetto At(Spare,Ground)
* LeaveOvernight, che ha come anche effetto not At(Flat,Axle). Ma il problema è che questa regola ha altri effetti, tra cui: 
    - not At(Spare, Ground) che è in contrapposizione con l'effetto di Remove(Spare,Trunk)
    - not At(Spare, Trunk) che è in contrapposizione con la precondizione di Remove(Spare, Trunk)

Abbiamo 2 tipi di interferenza:

1. Le 2 azioni hanno effetti opposti
2. L'effetto di un'azione distrugge una precondizione dell'azione successiva.

Il risultato è che LeaveOvernight non va bene come candidata perchè è in contrapposizione con l'altro azione, ma non può essere neanche messa prima perchè renderebbe non applicabile l'altra azione. Quindi non va bene e basta, non è una candidata.

### Graph plan

Per risolvere un problema come il mondo dei blocchi o gli aerei, non è facile trovare una buona euristica. Le euristiche sono **domain depending**, quindi per ogni diverso problema bisogna trovare una diversa euristica.

L'ideale sarebbe avere delle **euristiche non domain dependent**

Dominio: abbiamo una torta nello stato iniziale. Il goal è di aver mangiato il dolce.

    Init(Have(Cake) and not Eaten(Cake))
    Goal(Have(Cake) and Eaten(Cake))
    Action(Eat(Cake)
        Precond: Have(Cake)
        Effect: not Have(Cake) and Eaten(Cake))
    Action(Bake(Cake)
        Precond: not Have(Cake)
        Effect: Have(Cake))

Per il graph plan vedi figura 11.11 Russel Norvig 2 ed.

Il graph plan alterna un livello degli stati con un livello delle azioni e così via.

L'idea base di un Graph Plan è di partire da S_0 (lo stato iniziale).

In A0 vengono segnate tutte le azioni che sono applicabili nello stato iniziale. In questo caso l'unica azione è Eat(Cake). 
Però in figura si vedono anche dei quadratini vuoti. Questo perchè vengono prese in considerazione delle **pseudo-azioni di persistenza**. Cioè se è vero un certo fatto, quello persiste nel tempo.
Ciò che è vero nello stato iniziale viene portato avanti dalle **pseudo-azioni di persistenza**

Quindi nello stato S_1 c'è

    Have(Cake)
    not Have(Cake)
    Eaten(Cake)
    not Eaten(Cake)

Qua teoricamente avremmo già il goal. Ma ci sono delle interferenze di azioni, quindi non ci fermiamo.

In A_1 ci sono tutte le azioni che sarebbero applicabili dai fatti di S_1

A_1 produce tutti i fatti di S_2. In questo caso ci sono gli stessi fatti di S_1.

Ora possiamo fermarci. Capiamo perchè.

C'è un altro ingrediente fondamentale che non abbiamo visto: l'azione di **mutex** (mutual exclusion). Indica quali azioni (o fatti) non possono andare insieme.

In S_1 non ci eravamo fermati perchè Have(Cake) e Eaten(Cake) erano in **mutex** tra di loro. Quindi in verità il goal non era soddisfatto.

La regola è che se due azioni producono due effetti mutualmente esclusivi, allora anche le due azioni sono mutualmente esclusive.

Anche due fatti che sono stati generati da regole mutualmente esclusive sono mutex tra di loro.

In S_2 **potenzialmente** arrivo al goal. Si intende potenzialmente perchè le valutazioni che abbiamo fatto sono tutte locali. 

Il **graph plan** è una **euristica** per capire in quanti passi **potenzialmente** si possa arrivare al goal. Ci ha detto per esempio che in un passo era impossibile arrivare al goal. Era potenzialmente possibile in 2 passi.

Quindi durante la mia strategia di pianificazione posso utilizzare un graph plan come euristica. Una volta trovato un potenziale goal, posso partire da quel punto per fare una **regression** e tornare ad avere lo stato iniziale.

## 31 marzo

### Graphplan (continuaz.) fig. 9 cap. 10 Russel Norvig 3a ed. intl

Riprendiamo il dominio have(cake) eaten(cake)

Euristiche: 

* MAX LEVEL: è il max sui goal g dei level(g). Da tutti i singoli goal atomici vado a prendere quello con livello massimo. Non appena ho tutti i goal, suppongo che lì ci sia il livello della soluzione. In questo caso ritorna 1
* SUM LEVEL: sommatoria su tutti i g appartenenti a goals di level(g) - sum(level(g))
* Ritorna il livello più basso in cui tutti i goal sono presenti ma non sono in mutex tra di loro (la vera unica e sensata secondo il prof). In questo caso ritorna 2

Vediamo come la costruzione di un grafo di pianificazione può essere utilizzato non solo per fare euristiche ma proprio per costruire un piano.

Questo metodo combina un metodo di progressione creando un Graphplan con un metodo di regressione tornando indietro sull'albero. Vedi figura 10.9 Russel Norvig 2a ed.

Gli obiettivi sono i goal. Sono obiettivi congiunti (sono in and tra di loro).

cosa sono i **nogood**? Servono per tenere traccia che certe combinazioni non ammettono soluzione. Una combinazione di fatti di sottogoal non contiene soluzione. Queste combinazioni vengono labellati come *nogood*

**ESTRAI-SOLUZIONE** può ritornare un fallimento anche se si parte da un livello per cui nel graphplan c'erano tutti i goal non in mutex tra di loro. Potrebbe non esserci soluzione in quel livello.

Se abbiamo fallito la soluzione e l'ultima condizione del codice non è vera (vedi fig. 9) allora espando il grafo di un livello e rifaccio tutto.

*if grafo e nogood si sono livellati entrambi*: vuol dire che due livelli di uno stato sono esattamente uguali, cioè hanno gli stessi fatti e le stesse relazioni di mutex tra di loro. Vuol dire che andare avanti di un livello non ha importato alcuna informazione nuova, perchè non ha creato alcun fatto ma non ha neanche cambiato le condizioni di mutex. Questo è un criterio di terminazione per fallimento: non vale la pena andare avanti perchè non c'è più nulla di nuovo: andare avanti di un passo non cambierebbe rispetto al passo precedente.

Controlli che deve fare una strategia come graphplan:

* **Effetti inconsistenti**: un'azione mi produce il letterale A e un'altra azione mi produce il letterale opposto (not A). Queste due azioni non possono essere considerate nello stesso passo. Sono alternative tra di loro quindi dobbiamo metterle in mutex.
* **Competing needs** (l'opposto degli effetti inconsistenti): Due azioni devono essere considerate in mutex se una richiede come stato di partenza A e l'altra not A. Hanno dei vincoli che sono opposti. Non posso attivarle insieme perchè le precondizioni sono opposte tra di loro.
* **Interferenze**: abbiamo due azioni, e un'azione ha come precondizione A. L'altra azione ha come effetto not A. Questa situazione è più sottile delle altre. Perchè queste due azioni non vanno bene? Perchè sono contemplate nello stesso "istante di tempo".
* **Supporto inconsistente**: supponiamo di avere due azioni che hanno come effetto rispettivamente A e B. Se la prima azione è in mutex con la seconda, allora anche A è in mutex con B.

Vedi figura 11.14 su Russel Norvig 2 ed.

ESTRAI-SOLUZIONE fa il tentativo di trovare il piano facendo una regression. Non è detto che la soluzione sia un **piano totalmente ordinato**.

Nel nostro caso il piano trovato è questo (è parzialmente ordinato):

                   +------------+                           
    At(S,T)+-------> Remove(S,T)+--------+                  
                   +------------+    +---v------+     +----+
                                     |PutOn(S,A)+----->Goal|
                   +------------+    +---^------+     +----+
    At(F,A)+-------> Remove(F,A)+--------+                  
                   +------------+                           


#### Complessità

La costruzione del grafo ha complessità polinomiale.

La complessità viene da ESTRAI-SOLUZIONE. Questo perchè può fallire e perchè può fallire dopo aver provato tante strade. potenzialmente può provare un numero esponenziale di strade.

#### Considerazioni

Tenendo presente che l'insieme dei fatti tra un livello dello stato e il livello successivo cresce, c'è un'azione che può essere eseguibile a un livello A_i e non è eseguibile a livello A_i+1 ? No. L'insieme delle regole applicabili aumenta sempre.

Un'altra cosa che è meno facile da vedere è questa: se due azioni o due fatti sono messi in mutex a un livello, nel livello successivo i mutex aumentano o diminuiscono? Alcuni diminuiscono, specialmente tra le azioni. Nel caso have(cake) eaten(cake) nell'ultimo stato ci sono due fatti che prima erano in mutex e ora non lo sono più.

Graph plan è considerato lo stato dell'arte.

### Tipi di pianificazioni

* Pianificazione classica.
    - Satplan
    - Le assunzioni di base sono:
        + Conosco lo stato iniziale
        + Esiste un modello delle azioni, sia in termini di **precondizioni** che in termini di **effetti**. Gli effetti però devono essere **deterministici**. Non cambia mai l'effetto se l'azione applicata è la stessa. Purtroppo non sempre la realtà è deterministica.
        + Sono in grado di riconoscere lo stato finale.
        + L'unico ad agire nel mondo è l'agente. Non capita che nel mondo dei blocchi il braccio posiziona un blocco, va a prenderne un altro e non trova più il blocco di partenza. Nei giochi è completamente diverso, non siamo in ambito di pianificazione classica: viene invece preso in considerazione fin dall'inizio che c'è un avversario che cerca di metterti in difficoltà.
* Pianificazione gerarchica: la pianificazione classica dà un'estrema responsabilità al sistema automatica. L'utente fornisce solo lo stato iniziale, il goal e il modello delle azioni. Invece con la pianificazione gerarchica si rende il sistema consapevole di alcune cose in più. Si tratta di scomporre il problema in alcuni sottoproblemi. Il sistema così sa già che un problema va decomposto in un determinato modo. Può essere anche specificato l'ordine con cui risolvere i sottoproblemi. Gli stessi sottoproblemi possono essere decomposti in altri sottoproblemi. 

## 01 Aprile

Proviamo a partire dalla pianifiazione classica e man mano togliere alcune delle assunzioni che abbiamo fatto prima.

Dominio: mondo dell'aspirapolvere con due piastrelle.

Togliamo il vincolo del **determinismo**. Assumiamo per esempio che quando l'aspirapolvere pulisce, è anche possibile che il pavimento non venga fuori pulito. Il libro parla di questa situazione utilizzando il termine:

**Bounded indeterminacy**: non so quale sarà l'effetto dell'azione, ma comunque sarà uno degli n possibili effetti.

In questo caso la pianificazione non è solo una serie di azioni che vanno applicate per arrivare dallo stato iniziale al goal. Deve invece tenere conto del fatto che ci sono diversi possibili stati in cui si arriva dopo aver effettuato un'azione.

Quello che è importante capire è che l'indeterminismo non è sotto il controllo dell'agente. Il fatto che ci si possa trovare in diversi stati dopo un'azione è una cosa su cui l'agente non può farci niente, ma deve saper gestire.

Andremo quindi a costruire dei **piani condizionali**.

### Piani condizionali

Uno dei modi di affrontare questo problema è fare una ricerca su un grafo and-or. Il nostro problema è sempre lo stesso: Stato iniziale, Goal e Modello delle azioni. La differenza è che il modello delle azioni non è deterministico, quindi un'azione può mandare arbitrariamente in stati differenti.

Il risultato è che questo metodo crea un piano sequenziale che è molto simile a un semplice programma.

Però qui per terminare la stesura del piano non basta arrivare al goal. Perchè se arrivo al goal è sempre possibile che un'azione indeterministica mi mandi in un percorso diverso che non ho terminato di pianificare.

La ricerca and-or ritorna fallimento se proseguendo il percorso si ritorna ad uno stato che è lo stesso di uno stato antenato.

I rami indeterministici sono considerati in AND tra di loro, perchè non sono sotto il controllo dell'agente. Infatti se uno di essi ritorna il fallimento, allora tutti quei rami insieme vengono considerati come fallimento, perchè è una situazione in cui l'agente non può scegliere. L'agente invece deve essere sempre sicuro di poter arrivare al goal se sceglie una strada.

L'attività di **pianificazione** e di **esecuzione** sono due cose MOLTO diverse. 

* **La pianificazione** (quella di cui stiamo parlando) viene fatta off-line a tavolino. Il modello delle azioni mi predice come funzionerebbe il mondo in modo virtuale. Questo processo mi produce un piano. Da questo si capisce che l'ipotesi del **determinismo** è un'ipotesi molto forte.
* **L'esecuzione** è fatta a run-time utilizzando un piano

Perchè tutta questa pianificazione abbia un senso dobbiamo fare una grande assunzione: che il mondo sia **osservabile**. Perchè l'agente attraverso i suoi percettori deve essere in grado di capire in che stato si trova, altrimenti non può gestire il non-determinismo.

Riassumendo: Quando si può applicare questo approccio?

1. Abbiamo un non determinismo limitato: **bounded indeterminacy**
2. Il nostro **mondo è osservabile**

Quanto è facile creare un piano con un grafo and-or? E' complicato. Non solo perchè magari computazionalmente è complesso. Ma anche solo a livello statistico, dato che le azioni non deterministiche hanno effetti in AND tra di loro, basta che uno solo dei rami che partono dagli effetti fallisca perchè tutto quel sotto albero fallisca. Quindi le probabilità di riuscire ad ottenere un piano sono molto basse. Questo non perchè non ci sia nessun cammino che porti al goal, ma perchè non esiste un piano che per ogni ramo non deterministico abbia un cammino che arriva sicuramente al goal.

Per esempio per quanto riguarda i voli, se un volo viene cancellato è possibile che non ci sia un modo per raggiungere il goal. Se si prendesse in considerazione la cancellazione dei voli, non esisterebbe mai un piano che mi porti da una città all'altra, perchè uno dei rami non deterministici ha un fallimento.

Quindi è MOLTO probabile che non esista un piano. Però non provarci neanche non è sempre la situazione più furba.

Un passo successivo è togliere il vincolo dei cicli (esempio dell'aspirapolvere che non riesce ad andare a destra o a sinistra con sicurezza). Questa idea di rilasciare il vincolo della ciclicità ha senso se in un numero definito di passi tutti gli stati effetto occorrono. E' come dire che se lancio una moneta e ottengo più volte testa, ha senso continuare a provare perchè ho una certa probabilità di ottenere croce prima o poi. Ogni tanto conviene anche limitare la ciclicità. Per esempio nel caso di una macchina che non si accende, conviene provare un paio di volte ma poi fermarsi perchè evidentemente la batteria è scarica.

**Belief state**: se abbiamo una osservabilità parziale del mondo, abbiamo un'incertezza su alcune cose. Per esempio il vacuum cleaner può vedere solo la stanza in cui si trova. Non può sapere cosa c'è nell'altra stanza, quindi il suo belief state è quello in cui ci sono più stati di cui UNO è sicuramente lo stato reale. Per esempio se il robot sa che è ha sinistra e a sinistra è pulito, il suo belief state include due stati, uno in cui a destra è pulito e l'altro in cui a destra è sporco. Uno di questi due stati è vero di sicuro. Più è grande il belief state, più è grande l'ignoranza dell'agente rispetto al mondo. Uno dei punti chiave per ridurre questa ambiguità è avere la possibilità di osservare il mondo.

Il libro presenta una situazione **sensorless** cioè in cui il mondo è **completamente non osservabile**. Nel caso mostrato (aspirapolvere), si riesce comunque a creare un piano anche in mancanza di osservazione.

## 08 Aprile

Vediamo adesso qualche algoritmo di ricerca con questi **belief state**. Queste strategie non sono per niente semplici come quelle nello spazio degli stati.

### Esempio dei belief state SENZA osservazione (no sensori)

Il libro mostra un esempio in cui non c'è osservazione.

Non abbiamo direttamente gli stati, ma abbiamo i belief-state, che derivano da un'ignoranza parziale che ha l'agente sul mondo.

Il numero di stati possibili avevamo visto che poteva essere molto grande già nelle strategie classiche. Qua è ancora peggio perchè assumiamo l'ignoranza dell'agente. Nell'esempio del vacuum-cleaner il belief state iniziale contiene tutti i possibili stati fisici perchè l'ignoranza dell'agente è totale.

**belief state**: Se un problema P ha N stati, il problema senza sensori ha fino a 2^N stati, anche se molti potrebbero essere irraggiungibili dallo stato iniziale.

**Stato iniziale**: Se non si sa proprio niente, è l'insieme di tutti gli stati fisici in P

**Azioni**: Si possono utilizzare due modalità:

* *Unione*: Le azioni applicabili in un belief state sono l'**unione** delle azioni applicabili nei singoli stati fisici che ne fanno parte. Questa politica non è sicura (posso fare dei disastri nel mondo fisico, per esempio l'agente è a sinistra e lo faccio andare di nuovo a sinistra), ma è funzionale perchè hai sempre delle azioni attivabili.
* *Intersezione*: Le azioni applicabili in un belief sono l'**intersezione** delle azioni applicabili nei singoli stati fisici che ne fanno parte. Questa politica è molto più sicura, ma spesso non è applicabile perchè l'intersezione risulta in un insieme vuoto e non puoi applicare nessuna azione.

**Modello di transizione**: Per qualunque stato s appartenente al belief state b, vado a vedere quali stati s' si possono raggiungere applicando un'azione a. Quindi applicando l'azione a al belief state b, si raggiunge il belief state b' formato dall'insieme di tutti gli s'. In questo caso, b' è una **prediction** di b se applico l'azione a. `b' = Prediction(b,a).` 

### Esempio dei belief state con osservazione parziale

Caso del vacuum-cleaner. Il libro mostra due casi (a) e (b).

Nel caso (a) siamo in un modello deterministico delle azioni. Vado a destra e so che l'agente è andato a destra. L'ignoranza iniziale era riguardo al fatto che la stanza a destra fosse pulita o sporca. Una volta che il robot è andato a destra, può osservare se è pulito o sporco.

Nel caso (b) il modello non è deterministico perchè quando l'agente cerca di andare a destra non è sicuro che ci riesca.

Se vediamo i belief state come un'insieme di elementi in XOR tra di loro

Per esempio il belief state che contiene i due stati `a` e `b`, è descrivibile come `a XOR b`. Lo XOR a sua volta è descrivibile come `(a AND NOT b) OR (NOT a AND b)`

Per questo motivo le osservazioni che vengono dall'esterno sfoltiscono le nostre credenze: se l'agente osserva `NOT a`, sa che in quel belief state è valida `NOT a AND b`, quindi in definitiva `b` è l'unico stato attivabile.

Nel caso **deterministico**, il belief state risultante è sempre al massimo grande quanto quello di partenza, perchè le osservazioni non possono fare che ridurre il numero di stati possibili `|b'| <= |b|`. Se non si osserva, in ogni caso ogni stato fisico risulta in un solo stato fisico.

Nel caso **non deterministico**, il belief state risultante può essere anche più grande perchè ogni singolo stato fisico potrebbe avere più di uno stato risultante. Facendo un'osservazione dopo essere andato a destra, il robot riesce comunque a ridurre un po' di ambiguità. Si vede nello schema che se tutto va male il robot si ritrova esattamente al suo stato di partenza.

Il meccanismo che applico *senza osservazioni* è questo:

* Ho un **belief state**
* Ho alcune **azioni applicabili**
* Devo applicare un meccanismo di **predizione**, che produce per ogni azione:
    - **predizione** a_1
        + produce un **belief state** b_1
    - **predizione** a_2
        + produce un **belief state** b_2
    - ...

Il meccanismo che applico *con osservazioni* è questo:

* Ho un **belief state**
* Ho alcune **azioni applicabili**
* Devo applicare un meccanismo di **predizione**, che produce per ogni azione:
    - **predizione** a_1
        + produce un **belief state** b_1, costruito anche con delle **osservazioni**
    - **predizione** a_2
        + produce un **belief state** b_2, costruito anche con delle **osservazioni**
    - ...

Quello che cambia è che i **belief state** vengono creati da una fusione tra le **predizioni** e le **osservazioni**.

Nel libro, `Results(b,a) = Update(Prediction(b,a), o)`, dove

* **b** è il belief state di partenza
* **a** è l'azione che si vuole applicare
* **o** sono le osservazioni, dove `o appartiene-a Percezioni-Possibili(Predizione(b,a))`
* **Results** ritorna tutti i belief state che risultano da b applicando a
* **Update** aggiorna la predizione utilizzando le osservazioni
* **Prediction** è stato descritto un'ora fa con il *modello di transizione*.

#### Proposizionalizzazione

dal libro di Russel Norvig. Un modo per risolvere dei problemi è sostituire in tutti i modi possibili delle variabili quantificate universalmente. Funziona solo se non ho infiniti modi. I pianificatori di ultima generazione fanno una cosa di questo tipo.

Per esempio, prendendo il dominio della logistica, 

    move(?p, ?a1, ?a2)

Utilizzando la proposizionalizzazione, istanzio tutte le regole prima, non lo faccio a runtime. Per esempio se ho 10 aeroplani e 100 aeroporti, un pianificatore che utilizza la proposizionalizzazione istanzia a priori 100000 move.


### Strategie ONLINE

Possiamo applicare delle strategie di planning al problema del labirinto?

Il labirinto è fatto da una griglia. Ogni elemento della griglia può essere un muro (w), un elemento vuoto oppure un gate (G).

    |w|w|w|w|w|G|w|w|w|w|w|w|w|w|
    |w| |w| | | | | | |w| | | |w|
    |w| |w|w|w|w|w| | |w|w|w| |w|
    |w| |w| | | |w| | | | | | |w|
    |w| | | |w| | | |w|w|w|w|w|w|
    |w|w| |w|w| |w|w|w| | | | |w|
    |w| | | | | | | | | |w|w| |w|
    |w| | |w|w|w|w|w|w|w|w|w| |w|
    |w|w| |w| | | | | | | |w| |w|
    |w| | |w| | | | | | | |w| |w|
    |w| |w|w| |w|w|w|w|w| |w| |w|
    |w| | |w| |w| | | |w| |w| |w|
    |w|w| | | | | |w| |w| |w| |w|
    |w|w|w|w|w|w|w|w|G|w|w|w|w|w|

Lo scopo è partire dal gate in basso ed arrivare a quello in alto. è possibile eseguire una pianificazione? Sì, usando A* (a star)

Un altro esempio è quello in cui non ho neanche la mappa.

    | | | | | | | | | | | | | | |
    | | | | | | | | | | | | | | |
    | | | | | | | | | | | | | | |
    | | | | | | | | | | | | | | |
    | | | | | | | | | | | | | | |
    | | | | | | | | | | | | | | |
    | | | | | | | | | | | | | | |
    | | | | | | | | | | | | | | |
    | | | | | | | | | | | | | | |
    | | | | | | | | | | | | | | |
    | | | | | | | | | | | | | | |
    | | | | | | | | | | | | | | |
    | | | | | | | | | | | | | | |
    | | | | | | | | | | | | | | |

Senza neanche l'ambiente, gli approcci classici alla pianificazione non servono a nulla.

Sul libro si parla dell'**agente online**, cioè l'agente che non sta facendo una pianificazione prima di eseguire le azioni, ma sta eseguendo le azioni direttamente nel mono. Le assunzioni del libro sono che l'agente: 

* Non sa nulla dell'ambiente
* Non sa nulla degli effetti delle sue azioni
* Conosce le condizioni di applicabilità delle azioni
* È comunque in grado di sapere se è arrivato al goal

Quindi l'agente non conosce gli effetti delle sue azioni, ma li osserva.

È un agente che ha memoria, non è un agente a riflessi semplici.

## 09 Aprile

Vedere figura 4.21 del Russel Norvig 2a ed.

Una grossa differenza e' che non si puo' fare backtracking facendo finta di niente, perchè l'agente agisce davvero nel mondo. A volte non è possibile fare backtracking. Nel libro assume che l'agente si possa trovare in una situazione in cui non sa più cosa fare perchè si è infilato in una situazione che non va bene ma non ha possibilità di backtracking. In questo caso l'agente ritorna `stop`

L'agente deve anche scoprire quali sono le azioni che annullano le azioni precedenti. Per esempio deve scoprire che un'operazione di load si annulla con un'operazione di unload. In questo modo può capire quali sono le azioni che permettono di fare backtracking. Questo può funzionare perchè il mondo che stiamo considerando (il labirinto) è reversibile.

### Non classical planning (cap. 11 Russel Norvig 2a ed.)

Il **Classical planning** assume:

* Stato iniziale
    - dell'agente: conosciuto
    - dell'ambiente: conosciuto
* Azioni
    - Precondizioni conosciute
    - Effetti
        + deterministici
        + conosciuti
* Osservabilità dell'ambiente piena
* Goal conosciuto


Prendiamo le assunzioni del classical planning una ad una e modifichiamole 

#### Contingent planning - Azioni con effetti non deterministici

Prima ci siamo rimangiati il determinismo: abbiamo detto "non è sempre vero che gli effetti sono indeterministici". Abbiamo usato il concetto di **bounded indeterminacy**, cioè non so qual'è l'effetto ma l'indeterminatezza è limitata. Possono solo esserci alcuni effetti in alternativa tra di loro. Per risolvere questo problema abbiamo visto il **Contingent planning**, utilizzando dei **grafi and/or** e dei **piani condizionali**. 

#### Conformant planning - Osservabilità del mondo nulla o limitata

Se abbiamo un'**osservabilità parziale dell'ambiente**, possiamo utilizzare delle **sensing action**, che servono per scoprire com'è una parte del mondo. Abbiamo utilizzato dei **belief state** per descrivere l'ignoranza dell'agente rispetto al mondo. Le azioni di sensing sono delle azioni a tutti gli effetti, che vengono modellate e hanno delle precondizioni. Le azioni di sensing NON cambiano il mondo, ma cambiano lo stato di conoscenza dell'agente. Queste azioni possono far parte delle attività di pianificazione.

Nel caso dell'**osservabilità dell'ambiente nulla**, non ci sono sensing action. Abbiamo visto l'esempio del vacuum cleaner che nello stato iniziale ha un **belief state** con tutti gli stati fisici possibili.

Per risolvere questi problemi abbiamo utilizzato delle tecniche di **conformant planning** (che utilizzano i **belief state**).

Ci sono due problemi principali nel **conformant planning**

1. Spesso non si riesce a trovare un piano conformante che soddisfi tutti i possibili stati interni ai diversi belief state. In questo caso non esiste un piano
2. Quando si trova un piano, molto spesso è parecchio sub-ottimo.

#### Online agent 

Nel caso dell'**online agent** abbiamo parecchie cose diverse dal classical planning:

* Stato iniziale
    - dell'agente: conosciuto
    - **dell'ambiente: sconosciuto**
* Azioni
    - Precondizioni conosciute
    - Effetti
        + deterministici
        + **conosciuti**
* **Osservabilità dell'ambiente parziale**
* Goal conosciuto

### Ripianificazione

Abbiamo un problema (anche di classical planning) e abbiamo un piano da mandare in esecuzione.

Durante l'esecuzione del piano, per qualche motivo mi trovo in uno stato diverso da quello che prevedevo. Quindi **la predizione è diversa dall'osservazione**. In questo caso il piano che avevo fatto non può più funzionare.

L'idea è di ripianificare. E' come avere un nuovo problema che ha come stato iniziale lo stato in cui sono. Bisogna quindi trovare un altro piano. La situazione tipica (per i voli) è quando viene cancellato un volo.

La figura 12.14 fa vedere che è possibile fare qualcosa che sia meglio che ripianificare completamente da zero. Utilizza una tecnica di **plan repair** che serve per mettere una toppa sul piano. Volevo essere nello stato *P*, sono finito nello stato *O*, vedo se riesco a fare un **piano di riparazione** che mi manda da *O* a *P* e quando sono in *P* riprendo a seguire il piano iniziale.

La differenza qui è che mentre durante la fase di pianificazione iniziale non ho limiti di tempo, durante la ripianificazione o il plan repair, spesso ho dei limiti di tempo (come nel caso degli aerei cancellati), quindi ammesso che riesca a trovare una soluzione è anche probabile che non sia ottima.

## 14 Aprile

### Vincoli temporali

Fino ad adesso abbiamo fatto dei modelli delle azioni in cui le azioni sono atomiche, cioè sono indivisibili all'interno, e sono immediate. Per esempio l'azione di flight da un aeroporto all'altro l'abbiamo sempre considerata come atomica. Questa ipotesi non va sempre bene.

#### Job Scheduling

Vediamo un problema di job scheduling. Il dominio è la costruzione di un veicolo.

Il primo aspetto importante è che **non è vero che le azioni sono atomiche o hanno tutte la stessa durata**. Il secondo aspetto (un po' meno evidente) è il discorso delle **risorse**: dobbiamo tenere conto che svolgendo azioni consumiamo delle risorse. A volte si tratta di **risorse riutilizzabili**. Per esempio un ascensore è una risorsa riutilizzabile: non la puoi usare mentre la usa qualcun altro.

Ci sono due formule: **ES** e **LS** cioè **Earliest Start**e **Latest Time**. L'obiettivo in job scheduling è quello di comprimere la durata del mio piano il più possibile.

#### Hierarchical Task Network (HTN)

L'idea che sta sotto a questo tipo di approccio (che è stato parecchio utilizzato per esempio per le sonde su marte) è questa: in molti casi non si conosce il piano, ma abbiamo una qualche idea della sequenza dei principali passi che dobbiamo fare. Si hanno quindi una serie di **task** che sappiamo di dover fare in sequenza per risolvere il problema.

Ognuno di questi **task** può essere **decomposto** in **sottotask**.

Ogni task può essere svolto anche in diversi modi. L'esempio del libro è quello di andare da casa a honolulu.

La prima azione da fare è andare da casa all'aeroporto. Questa può essere decomposta in un'unica azione atomica che è prendere il taxi.

Il prof dice che questo esempio è brutto perchè non vengono messe delle precondizioni per scomporre il task in sottotask. Per esempio se devo prendere il taxi una precondizione è quella di avere dei soldi dietro. Se invece voglio risolvere il task guidando la mia macchina, la precondizione è avere la macchina disponibile con del carburante.

Un altro esempio è quello di navigazione di un robottino all'interno di un labirinto. Come esempio va un po' meglio e fa capire come deve essere decomposto un problema.

Ci sono 3 **raffinamenti** diversi del **task** naviga. Il task naviga ti fa navigare dal punto [a,b] al punto [x,y].

1. Precondizione: a=x, b=y. Azione: nulla
2. Precondizione: Connesso([a,b], [a-1,b]). Azione: vai a sinistra e naviga([a-1,b], [x,y])
3. Precondizione: Connesso([a,b],[a+1,b]). Azione: vai a destra e naviga([a+1,b], [x,y])

## 15 aprile

### Labirinto

Vedi la cartella maze.