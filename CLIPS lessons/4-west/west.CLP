(defrule is_criminal
  (american ?x)
  (weapon ?y)
  (sells ?x ?y ?z)
  (hostile ?z)
  => (assert (criminal ?x)))

(defrule all_missiles_sold_by_west
  (missile ?x)
  (owns nono ?x)
  => (assert (sells west ?x nono)))

(defrule missile_is_weapon
  (missile ?x)
  => (assert (weapon ?x))) 

(defrule hostile
  (enemy ?x america)
  => (assert (hostile ?x)))

(defrule solution
  (goal criminal ?y)
  (criminal ?y)
  => (printout t ?y " is a criminal " crlf) 
    (halt))

(deffacts initial
  (american west)
  (enemy nono america)
  (owns nono m1)
  (missile m1)
  (goal criminal west))