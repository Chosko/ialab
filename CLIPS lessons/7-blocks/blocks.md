Queste regole sono scritte in STRIPS per il mondo dei blocchi.

    pick (x, y)
    IF   on (x, y) and   clear(x) and handempty
    THEN add( clear(y))
        add (holding (x))
        delete (on (x, y))
        delete (clear(x))
        delete (handempty)   
        
    picktable (x)
    IF   ontable(x)  and   clear(x) and handempty
    THEN add (holding (x))
        delete (ontable(x))
        delete (clear(x))
        delete (handempty)   
     
    put (x,y)
    IF holding (x) and   clear(y) 
    THEN  delete (clear(y))
        delete (holding (x))
        add (on (x, y))
        add (clear(x))
        add (handempty)   
        
    puttable(x)
    IF holding (x ) 
    THEN delete (holding (x))
        add (ontable(x))
        add (clear(x))
        add (handempty) 

La modellizzazione di queste regole è molto simile alle regole di CLIPS

Descrive questo mondo:

|---|  |---|   
| A |  | C |   
|---|  |---|   |---|
| B |  | D |   | E |
|---|  |---|   |---|

Il goal è questo

|---|
| A |
|---|
| B |
|---|
| E |
|---|

### Traduciamo le regole in CLIPS, prendendo spunto da Logistics

#### pick

prendiamo spunto da **move** in Logistics

    (defrule pick
        (status ?s clear ?x NA)
        (status ?s on ?x ?y)
        (status ?s handempty NA NA)
        (maxdepth ?d) ; identica a move
        (test (< ?s ?d)) ; identica a move
        (not (exec ?s pick ?x ?)) ; copiata da move, ma modificata
        => (assert (apply ?s pick ?x ?y)))

prendiamo **apply-move1** e **apply-move2** cambiando move con pick. Queste due regole eliminano tutti i fatti status e exec riferiti agli stati più profondi che sono stati visitati ma non fanno parte della soluzione.

prendiamo spunto da **apply-move3** in Logistics

    (defrule apply-pick3
        ?f <- (apply ?s pick ?x ?y)
        =>
        (retract ?f)
        (assert 
            (delete ?s on ?x ?y)
            (delete ?s clear ?x NA)
            (delete ?s handempty NA NA)
            (status (+ ?s 1) holding ?x NA)
            (status (+ ?s 1) clear ?y NA)))

Nella variabile f viene messo l'identificatore del fatto che soddisfa l'apply richiesta

Le righe con (status ...) asseriscono i fatti che determinano lo stato successivo. Il braccio sta tenendo ?x e il blocco ?y è ora vuoto sopra.

#### Modulo CHECK
Vediamo il modulo **CHECK**, cosa serve e cosa non serve?

si possono tenere TUTTE Le regole

#### Modulo NEW

si possono tenere TUTTE le regole

#### Modulo DEL

si può tenere tutto uguale